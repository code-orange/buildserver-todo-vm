#!/bin/bash

sh update_submodules.sh

cd meta-images
urls=`git submodule foreach git config --get remote.origin.url | grep -E '(http|https)://'`

echo "node: {" > ../Jenkinsfile
echo "    stage 'build'" >> ../Jenkinsfile

while read -r line; do
    if [[ $line =~ "centos" || $line =~ "netstack-router-legacy" ]]
    then
       echo "Backlist entry found: $line"
       continue
    fi
    echo "    build job: 'vm_image_build', propagate: false, parameters: [[\$class: 'StringParameterValue', name: 'VM_GIT_URL', value: '$line']]" >> ../Jenkinsfile
done <<< "$urls"

echo "}" >> ../Jenkinsfile

cd ..

git add Jenkinsfile

git commit -a -m "update Jenkinsfile"
git push origin master
